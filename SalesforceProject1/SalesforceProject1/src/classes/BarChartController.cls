public class BarChartController 
{
    static List<DataWrapper> mydata=new List<DataWrapper>();
	public static Integer dataInput1 {get; Set;}
	Public static Integer DataInput2 {get; set;}
	Public static Integer datainput3 {get; set;}
	public static Integer dataInput4 {get; set;}


	Public BarChartController()
	{
		dataInput1= 0;
		dataInput2= 0;
		dataInput3= 0;
		dataInput4 = 0;
	}

	Public List<DataWrapper> getData()
	{
		mydata.add(new DataWrapper('2012',dataInput1));
		mydata.add(new DataWrapper('2013',dataInput2));
		mydata.add(new DataWrapper('2014',dataInput3));
		mydata.add(new DataWrapper('2015',dataInput4));

		return mydata;
	}

	public class DataWrapper
	{
		public string year{get; set;}
		public integer growthRate {get; set;}

		public DataWrapper(String year, Integer growthRate)
		{
			this.year=year;
			this.growthRate=growthRate;
		}
	}

}